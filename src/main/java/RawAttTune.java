import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
 * Created by msiegfried on 11/23/16.
 */
public class RawAttTune implements Serializable {
    private static final int RECORD_LENGTH = 11;
    private static Logger log = LoggerFactory.getLogger(RawAttTune.class);
    private static SimpleDateFormat dateFormat  = new SimpleDateFormat("YYYYMMDDHHMMSSmmm");

    // Record Fields
    private String recordType;
    private String televisionReceiverId;
    private Long channelTuneStartTimestamp;
    private Long channelTuneEndTimestamp;
    private String contextDependentContentId;
    private String accessChannelNumber;
    private String eventStatusCode;
    private String displayTypeCode;
    private String servicePrecedenceTypeCode;
    private String channelTypeCode;
    private String tuneId;


    private static String[] splitRecord(String text, int... offsets) {
        String[] result = new String[offsets.length];
        int pos = 0;
        for (int i = 0; i < offsets.length; i++) {
            int start = i > 0 ? pos : 0;
            int end = pos + offsets[i];
            pos += offsets[i];
            result[i] = text.substring(start, end);
        }
        return result;
    }

    public static RawAttTune convertTune(String line)
    {
        RawAttTune tune = new RawAttTune();

        String[] cols;
        try {
            cols = splitRecord(line, 1, 36, 17, 17, 36, 5, 1, 3, 3, 60, 18);
        } catch (StringIndexOutOfBoundsException e) {
            log.error(e.getMessage());
            return tune;
        }

        if (cols.length < RECORD_LENGTH) {
            log.error("Record length was " + cols.length + " expected " + RECORD_LENGTH);
            return tune;
        }

        tune.setRecordType(cols[0]);
        tune.setTelevisionReceiverId(cols[1]);

        if (StringUtils.isNotBlank(cols[2])) {
            try {
                tune.setChannelTuneStartTimestamp(dateFormat.parse(cols[2]).getTime());
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        if (StringUtils.isNotBlank(cols[3])) {
            try {
                tune.setChannelTuneEndTimestamp(dateFormat.parse(cols[3]).getTime());
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        tune.setContextDependentContentId(cols[4]);
        tune.setAccessChannelNumber(cols[5]);
        tune.setEventStatusCode(cols[6]);
        tune.setDisplayTypeCode(cols[7]);
        tune.setServicePrecedenceTypeCode(cols[8]);
        tune.setChannelTypeCode(cols[9]);
        tune.setTuneId(cols[10]);

        return tune;
    }

    public String getRecordType() {
        return recordType;
    }

    public void setRecordType(String recordType) {
        this.recordType = recordType;
    }

    public String getTelevisionReceiverId() {
        return televisionReceiverId;
    }

    public void setTelevisionReceiverId(String televisionReceiverId) {
        this.televisionReceiverId = televisionReceiverId;
    }

    public Long getChannelTuneStartTimestamp() {
        return channelTuneStartTimestamp;
    }

    public void setChannelTuneStartTimestamp(Long channelTuneStartTimestamp) {
        this.channelTuneStartTimestamp = channelTuneStartTimestamp;
    }

    public Long getChannelTuneEndTimestamp() {
        return channelTuneEndTimestamp;
    }

    public void setChannelTuneEndTimestamp(Long channelTuneEndTimestamp) {
        this.channelTuneEndTimestamp = channelTuneEndTimestamp;
    }

    public String getContextDependentContentId() {
        return contextDependentContentId;
    }

    public void setContextDependentContentId(String contextDependentContentId) {
        this.contextDependentContentId = contextDependentContentId;
    }

    public String getAccessChannelNumber() {
        return accessChannelNumber;
    }

    public void setAccessChannelNumber(String accessChannelNumber) {
        this.accessChannelNumber = accessChannelNumber;
    }

    public String getEventStatusCode() {
        return eventStatusCode;
    }

    public void setEventStatusCode(String eventStatusCode) {
        this.eventStatusCode = eventStatusCode;
    }

    public String getDisplayTypeCode() {
        return displayTypeCode;
    }

    public void setDisplayTypeCode(String displayTypeCode) {
        this.displayTypeCode = displayTypeCode;
    }

    public String getServicePrecedenceTypeCode() {
        return servicePrecedenceTypeCode;
    }

    public void setServicePrecedenceTypeCode(String servicePrecedenceTypeCode) {
        this.servicePrecedenceTypeCode = servicePrecedenceTypeCode;
    }

    public String getChannelTypeCode() {
        return channelTypeCode;
    }

    public void setChannelTypeCode(String channelTypeCode) {
        this.channelTypeCode = channelTypeCode;
    }

    public String getTuneId() {
        return tuneId;
    }

    public void setTuneId(String tuneId) {
        this.tuneId = tuneId;
    }
}

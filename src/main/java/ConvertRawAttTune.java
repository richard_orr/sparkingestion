import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SaveMode;
import org.apache.spark.sql.SparkSession;

/**
 * Created by msiegfried on 11/22/16.
 */

public class ConvertRawAttTune {
    public static void main(String[] args) {
        SparkSession spark = SparkSession
                .builder()
                .appName("Java Spark SQL basic example")
                .config("spark.sql.parquet.compression.codec", "gzip")
                .getOrCreate();

        JavaRDD<RawAttTune> tuneRDD = spark.read()
                .textFile("INCREMENTAL_CHNL_TUNE171.20161001164957426.log.gz")
                .javaRDD()
                .map(RawAttTune::convertTune);

        Dataset<Row> tuneDF = spark.createDataFrame(tuneRDD, RawAttTune.class);
        tuneDF.write().mode(SaveMode.Overwrite).parquet("rawTune.parquet");
    }
}
